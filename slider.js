function Slider (sliderContainer, elemWidth, elemNumber, showNumberContainer) {
    this.container = document.getElementsByClassName(sliderContainer)[0];
    this.width = elemWidth;
    this.marginL = 0;
    this.sliderWidth = elemWidth*(elemNumber-1);
    this.showNumberContainer = document.getElementsByClassName(showNumberContainer)[0];
    this.number = 1;
    this.showNumberContainer.innerHTML = `${this.number}`
    if (this.marginL==0) {
        document.getElementsByClassName('fa-angle-double-right')[0].classList.add('active');
    }
    this.left = function() {
        if (this.marginL<0) {
        this.marginL += elemWidth;
        this.container.style.marginLeft = this.marginL + 'px';
        this.number--;
        this.showNumberContainer.innerHTML = `${this.number}`
        } else {
            document.getElementsByClassName('fa-angle-double-left')[0].classList.remove('active');
        }
        if (this.marginL<0) {
            document.getElementsByClassName('fa-angle-double-right')[0].classList.add('active');
        }
    }
    this.right = function() {
        if(this.marginL > -this.sliderWidth) {
        this.marginL -= elemWidth;
        this.container.style.marginLeft = this.marginL + 'px';
        this.number++;
        this.showNumberContainer.innerHTML = `${this.number}`
        } else {
            document.getElementsByClassName('fa-angle-double-right')[0].classList.remove('active');
        }
        if (this.marginL > -this.sliderWidth) {
            document.getElementsByClassName('fa-angle-double-left')[0].classList.add('active');
        }
    }
}
var slider = new Slider('slider-container', 305, 4, 'showNumbers');

function toLeft() {
    slider.left();
};
function toRight() {
    slider.right();
};