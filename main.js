function openContainer (oldClass, Newclass) {
    document.getElementsByClassName(oldClass)[0].classList.toggle(Newclass);
};
function rotate180(elemClass, elemClass180) {
    document.getElementsByClassName(elemClass)[0].classList.toggle(elemClass180);
};
function openBlock1() {
    openContainer ('container', 'container-show');
    rotate180('fa1', 'rotate1')
};
function openBlock2() {
    openContainer ('container2', 'container2-show');
    rotate180('fa2', 'rotate2')
};
function openBlock3() {
    openContainer ('container3', 'container3-show');
    rotate180('fa3', 'rotate3')
};
function openTrain() {
    document.getElementsByClassName('train-container')[0].classList.toggle('open-works-train');
}
function openWorks() {
    document.getElementsByClassName('works-container')[0].classList.toggle('open-works-train');
}